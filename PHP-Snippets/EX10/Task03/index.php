<?php
// a number, a boolean, and based on condition change the result of the boolean ...after if statement print to screen
$name = "Jeff";
$age = 33;
$isFeelingOld = false;

if ($age > 50 )
{
    $isFeelingOld = true;
}

echo "It is $isFeelingOld that $name is feeling old at $age";

?>